﻿
     
     function bingGetAdvancedMap(apikey,id,where,data,hasloc,dashboardMode, showMiniMap, hideScalebar, viewStyle)
     {
        
        var map = new VEMap(id);
        map.SetCredentials(apikey);
        
        map.onLoadMap = function(){
        //map.ShowMessageBox = false;
        
        
        if(!hasloc && (data == null))
        {
           map.Find("",where,VEFindType.Businesses, null, 0, 1, true, true, true, true, null); 
        }
         if(hasloc)
        {
            var layerloc = new VEShapeLayer();
            var veLayerSpec = new VEShapeSourceSpecification(VEDataType.GeoRSS, '?xml=true', layerloc);
            
            map.ImportShapeLayerData(veLayerSpec,
                function(layer)
                {    

                      var numShapes = layer.GetShapeCount();
                      var shape;
                      for (var i = 0; i < numShapes; ++i)
                      {
                          shape = layer.GetShapeByIndex(i);
                          shape.SetCustomIcon(shape.IconId);
                      }
                }

            
            , 1);
        }
        
        if(data !=null)
        {
                var dataCount = 0;
                var dataLength = data.length;
                for(var i=0; i < data.length; ++i)
                {
                    
                
                        var newlayer = new VEShapeLayer();
                        newlayer.SetTitle(data[i][1]);
	                    map.AddShapeLayer(newlayer);
                        map.Find(data[i][0],where,VEFindType.Businesses, newlayer, 0, 20, true, true, true, true, 
                              function (layer, resultsArray, places, hasMore, veErrorMessage)
                              {
                                dataCount++;
                                
                                if(layer.GetTitle() != '')
                                {
      	                            var shapeCount = layer.GetShapeCount();
		                            for(var j=0; j < shapeCount; ++j)
		                            {
		                                var shape = layer.GetShapeByIndex(j);
                                        shape.SetCustomIcon(layer.GetTitle());
		                            }
		                        }  
		                        
		                        var locations = new Array();
                                        var numShapes = layer.GetShapeCount();
		                                for(var i=0; i < numShapes; ++i){
			                                var s = layer.GetShapeByIndex(i);
			                                 locations.push(s);
		                                }
                                        if (locations.length != 0)
                                            map.SetMapView(locations);
                                          
            		
            		            
            		
                              } 
                        
                            
                        
                        );
                    
                    
                }
        }
        
     }
     
     
switch(dashboardMode.toLowerCase())
         {
            case "normal":
                map.SetDashboardSize(VEDashboardSize.Normal);
                break;
            case "small":
                map.SetDashboardSize(VEDashboardSize.Small);
                break;
            case "tiny":
                map.SetDashboardSize(VEDashboardSize.Tiny);
                break;
            case "hide":
                map.HideDashboard();
                break;            
         
         }
     
 map.LoadMap();
 
 
 if(showMiniMap) map.ShowMiniMap();
                if(hideScalebar) map.HideScalebar();
                
                        switch(viewStyle.toLowerCase())
                 {
                    case "road":
                        map.SetMapStyle(VEMapStyle.Road);
                        break;
                    case "aerial":
                        map.SetMapStyle(VEMapStyle.Aerial);
                        break;
                    case "birdseye":
                        map.SetMapStyle(VEMapStyle.Birdseye);
                        break;
                
                 }

}



      