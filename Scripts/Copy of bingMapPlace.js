﻿      function bingMapSetLatLong()
	  {     
	        var map = new VEMap('myMap');
            map.LoadMap();
            
	  		map.Find('', $(".bingMapAddress").val(),VEFindType.Businesses, null, 0, 1, true, true, true, true, bingMapSetLatLong_callback);
	  }
	  
	  function bingMapSetLatLong_callback(layer, resultsArray, places, hasMore, veErrorMessage)
      {
          if(places == null)
          {
            $(".bingMessage").text("The place you have entered could not be found.").css("color","red");
          
            return;
          }
        var placeLatLong = places[0].LatLong + "";  
        var longlat = placeLatLong.split(",");
        $(".bingMapLatitude").val(longlat[0]);
        $(".bingMapLongitude").val(longlat[1]);
        $(".bingMessage").text("Longitude and Latitude were set successfully.").css("color","green");;
      }