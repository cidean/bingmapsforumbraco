﻿
      function bingGetBasicMap(apikey,mapid,where,whereTitle,whereDescription,what,icon,hideDashboard)
      {
         var map = null;  
         map = new VEMap(mapid);
         map.SetCredentials(apikey);
         map.onLoadMap = function(){};
         map.LoadMap();
         
         if(hideDashboard)
		    map.HideDashboard();

         FindLoc(mapid,map,where,whereTitle,whereDescription,what,icon,hideDashboard);  
      }   
      
      function FindLoc(mapid,map,where,whereTitle,whereDescription,what,icon,hideDashboard)
      {   
		    map.Find(null, where,VEFindType.Businesses, null, 0, 1, true, true, true, false, 
                function(layer, resultsArray, places, hasMore, veErrorMessage)
                {
                    var center = places[0].LatLong;
                    
                    var pin = new VEShape(VEShapeType.Pushpin, center);
                    pin.SetTitle(whereTitle);
                    pin.SetDescription(whereDescription);                      
                    map.AddShape(pin);
                    
                    map.Find(what, where,VEFindType.Businesses, null, 0, 20, true, true, true, true, 
                    
                    function(layer, resultsArray, places, hasMore, veErrorMessage)
                    {
                        if(places != null)
                        {
                            var locations = new Array();
                            var numShapes = layer.GetShapeCount();
		                    for(var i=0; i < numShapes; ++i){
			                    var s = layer.GetShapeByIndex(i);
			                    if(icon != "")
			                     s.SetCustomIcon(s.IconId);
			                     locations.push(s);
		                    }
                            if (locations.length != 0)
                                map.SetMapView(locations);    
                          }      
                    }
                    
                    
                    );
                }
            
            );
		   

      }
      

